@extends('backend.layouts.app')

@section('title', __('User Management'))

@section('breadcrumb-links')
    @include('backend.auth.user.includes.breadcrumb-links')
@endsection

@section('content')
    <table id="table_id" class="display" style="width:100%">
        <thead>
        <tr>
            <th>Name</th>
            <th>Email</th>
            <th>Active</th>
            <th>Created At</th>
        </tr>
        </thead>
    </table>
@endsection

@section('js')
    <script
            src="https://code.jquery.com/jquery-3.5.1.min.js"
            integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
            crossorigin="anonymous"></script>
    <script
            src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"
            integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU="
            crossorigin="anonymous"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.23/css/jquery.dataTables.css">
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.js"></script>
    <link href="https://cdn.datatables.net/buttons/1.6.5/css/buttons.dataTables.min.css" rel="stylesheet">

    <script type="text/javascript" src="https://cdn.datatables.net/1.10.23/js/dataTables.jqueryui.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.5/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/select/1.3.1/js/dataTables.select.min.js"></script>
    <script>
        $(document).ready(function () {
            var data = @json($data);
            data = data.original.data;
            var table = $("#table_id").DataTable({
                dom: 'Bfrtip',
                buttons: [
                    {
                        text: 'Deactivate',
                        action: function (e, dt, node, config) {
                            let data = table.rows({selected: true}).data();
                            let count = table.rows({selected: true}).data().length;
                            let users = [];
                            if (count) {
                                for (i = 0; i < count; i++) {
                                    users.push(data[i].id);
                                }
                            }
                            $.ajax({
                                url: '/admin/auth/user/bulk-deactivate',
                                type: "POST",
                                data: {'users': users, '_token': "{{csrf_token()}}"},
                                success: function (data, textStatus, jqXHR) {
                                    location.reload();
                                },
                                error: function (jqXHR, textStatus, errorThrown) {
                                }

                            });
                        }
                    },
                    {
                        text: 'Delete',
                        className: 'btn btn-info btn-sm',
                        action: function (e, dt, node, config) {
                            let data = table.rows({selected: true}).data();
                            let count = table.rows({selected: true}).data().length;
                            let users = [];
                            if (count) {
                                for (i = 0; i < count; i++) {
                                    users.push(data[i].id);
                                }
                            }
                            if (confirm("Are you sure you want to delete this users?")) {
                                $.ajax({
                                    url: '/admin/auth/user/bulk-delete',
                                    type: "POST",
                                    data: {'users': users, '_token': "{{csrf_token()}}"},
                                    success: function (data, textStatus, jqXHR) {
                                        location.reload();
                                    },
                                    error: function (jqXHR, textStatus, errorThrown) {
                                    }

                                });
                            }

                        }
                    }
                ],
                data: data,
                select: {
                    style: 'multi'
                },
                columns: [
                    {
                        "data": "name",
                    },
                    {
                        "data": "email",
                    },
                    {
                        "data": "active"
                    },
                    {
                        "data": "created_at",
                    }
                ],

            });
        });
    </script>
@endsection
